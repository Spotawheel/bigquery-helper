# BigQuery Helper

Last update: 2021-02-09

## Installation

You can install the package via composer:

```bash
"repositories": [
    {
        "type": "git",
        "url":  "https://bitbucket.org/Spotawheel/bigquery-helper.git"
    }
],
"require": {
    "spotawheel/bigquery-helper": "*"
},
```

The package will automatically register itself.

## Usage

You can publish the package's configuration using this command:

```bash
php artisan vendor:publish --provider="Spotawheel\BigQueryHelper\BigQueryHelperServiceProvider"
php artisan vendor:publish --provider="SchulzeFelix\BigQuery\BigQueryServiceProvider"
```

## Available functions

```
use Spotawheel\BigQueryHelper\BigQueryHelper;
$helper = new BigQueryHelper();

// Delete table(s)
$bigQuery->deleteTables(['users']);

// Sync all tables
$bigQuery->sync();

// Sync specific table(s)
$bigQuery->sync(['users', 'extras']);

// Push new rows without updating
$bigQuery->push();

// Get the logs for current job
$logs = $bigQuery->getLogs();
```

## Next steps and known issues
Our main issue is what happens when the DB Schema is changed. 
Now, we drop and re-create the tables that includes modified columns.
We should find a solution to change the schema and update only the new data in existing rows. 