<?php

namespace Spotawheel\BigQueryHelper;

use Carbon\Carbon;
use Google\Cloud\Core\ExponentialBackoff;
use Illuminate\Support\Facades\DB;
use SchulzeFelix\BigQuery\BigQueryFacade;

class BigQueryHelper
{
    private $logs;
    private $config;
    private $dataset;

    /**
     * BigQueryHelper constructor.
     *
     * @param  array  $configuration
     * @param  string  $database_connection_name
     */
    public function __construct($configuration = null, $database_connection_name = null)
    {
        $this->setConfiguration($configuration, $database_connection_name);
    }

    /**
     * Check if dataset exists, and if not create a new one.
     */
    private function checkDatasetExistance()
    {
        if (!BigQueryFacade::dataset($this->config['dataset_id'])->exists()) {
            \BigQuery::createDataset($this->config['dataset_id'], ['location' => 'EU']);
        }
    }

    private function setDataset()
    {
        $this->checkDatasetExistance();
        $this->dataset = BigQueryFacade::dataset($this->config['dataset_id']);
    }

    private function setConfiguration($configuration = null, $database_connection_name = null)
    {
        if ($configuration == null) {
            $configuration = config('bigquerycnfg');
        }

        $this->config['database_connection_name'] = $database_connection_name;
        $this->config['project_id'] = $configuration['project_id'];
        $this->config['dataset_id'] = $configuration['dataset_id'];
        $this->config['database_driver'] = $this->getDatabaseDriver();
        $this->config['excluded_tables'] = $configuration['excluded_tables'];
        $this->config['table_meta'] = $configuration['table_meta'];

        $this->setDataset();
    }

    public function getLogs()
    {
        return $this->logs;
    }

    private function getDatabaseDriver()
    {
        if ($this->config['database_connection_name'] == null) {
            return \DB::connection()->getPDO()->getAttribute(\PDO::ATTR_DRIVER_NAME);
        }

        return config("database.connections.{$this->config['database_connection_name']}.driver");
    }

    /**
     * Execute the console command.
     *
     * @param  array  $tables
     * @param  null  $maxKey
     * @param  bool  $reset_tables
     */
    public function sync($tables = [], $maxKey = null, $reset_tables = false)
    {
        $excluded_tables = $this->config['excluded_tables'];
        $table_suffix = '_tmp';

        /*
         * Get tables list.
         */
        if (empty($tables)) {
            $tables = $this->_getTablesList();
        }

        foreach ($tables as $table_name) {
            print("[$table_name] start ...");
            $chunks = $this->config['table_meta'][$table_name]['chunks'] ?? 150000;

            $this->logs[] = "[$table_name] start ...";

            $schema = $this->_getTableSchema($table_name);
            $bookmark_key = $this->_getBookmarkKey($table_name, $schema);
            $fields_list = array_column($schema['fields'], 'name');

            $tableExists = $this->dataset->table($table_name)->exists();

            /**
             * If table should be excluded and exists, delete it from BG.
             */
            $shouldBeExcluded = in_array($table_name, $excluded_tables);
            if ($shouldBeExcluded) {
                if ($tableExists) {
                    $this->logs[] = "[$table_name] deleting, should not exist ...";
                    $this->dataset->table($table_name)->delete();
                }
                continue;
            }

            /*
             * If table exists and schema changed, delete it
             */
            if ($tableExists) {
                $table_info = $this->dataset->table($table_name)->info();
                $schemaChanged = $this->_checkSchemaChanged($schema, $table_info['schema']);
                if ($schemaChanged) {
                    $this->logs[] = "[$table_name] deleting, schema changed ...";
                    $this->dataset->table($table_name)->delete();
                    $tableExists = false;
                }
            }

            /*
             * If table does not exist, create it
             */
            if (!$tableExists) {
                $this->logs[] = "[$table_name] creating ...";
                $this->dataset->createTable($table_name, ['schema' => $schema]);
            }

            /*
             * Get last entry key
             */
            $this->logs[] = "[$table_name] getting last entry ...";
            if ($maxKey == null) {
                $maxKey = $this->getLastEntry($table_name, $bookmark_key);
            }

            /*
             * Starting loop
             */
            $this->logs[] = "[$table_name] starting json chunks import ...";

            $order_by_column = $this->_getSortColumn($table_name, $bookmark_key);
            $offset_config = $this->_getOffsetConfig($table_name);

            DB::table($table_name)
                ->when($maxKey !== null && !$reset_tables,
                    function ($query) use ($maxKey, $bookmark_key, $offset_config) {
                        return $query->where($bookmark_key, '>', $maxKey)
                            ->when($offset_config !== false, function ($query) use ($offset_config) {
                                $offsetTime = $offset_config['time'];

                                if (is_string($offsetTime)) {
                                    $offsetTime = Carbon::parse($offsetTime);
                                }

                                if ($offset_config['time'] instanceof Carbon) {
                                    $offsetTime = $offset_config['time'];
                                }

                                $query->where($offset_config['column'], '<', $offsetTime);
                            });
                    })
                ->orderBy($this->_getSortColumn($table_name, $bookmark_key))
                ->orderBy('id')
                ->chunk($chunks, function ($data_part) use ($table_name, $table_suffix, $schema, $fields_list) {
                    $this->logs[] = count($data_part).' records found.';

                    /**
                     * We run the merge procedure per chunk instead of per table as we did in the past. 
                     * Doing that change we successfully resolved many issues regarding sync.
                     */
                    /**
                     * Delete temp table and create a clean one
                     */
                    $this->deleteTable($table_name.$table_suffix);
                    $this->createTable($table_name.$table_suffix, $schema);

                    /**
                     * Import data to temp table
                     * ND JSON is a specific type of json, google requires.
                     */
                    $nd_json = '';
                    foreach ($data_part as $item) {
                        $nd_json .= json_encode($item, JSON_UNESCAPED_UNICODE)."\r\n";
                    }

                    $are_data_loaded = $this->tryToLoadData($table_name, $table_suffix, $nd_json);

                    /**
                     * Merge data into
                     */
                    if ($are_data_loaded) {
                        $this->logs[] = "[$table_name] starting merge to main table ...";
                        $is_table_merged = $this->tryToMerge($table_name, $table_suffix, $fields_list);
                        if ($is_table_merged) {
                            $this->logs[] = "[$table_name] merged successfully ...";
                        } else {
                            $this->logs[] = "[$table_name] MERGE ERROR ...";
                        }
                    } else {
                        $this->logs[] = "BigQuery: Error before merging $table_name";
                        \Log::error("BigQuery: Error before merging $table_name");
                    }
                });

            /*
             * Remove Temp Table
             */
            $this->deleteTable($table_name.$table_suffix);

            $this->logs[] = "[$table_name] end ...";

            $maxKey = null;
        }

        $this->logs[] = 'Job ends here.';
    }

    /**
     * Execute the console command.
     *
     * @param  array  $tables
     * @param  bool  $reset_tables
     */
    public function push($tables = [], $reset_tables = false)
    {
        $chunks = 150000;
        $excluded_tables = array_merge($this->config['excluded_tables'], $this->config['do_not_delete_excluded']);

        /*
         * Get tables list.
         */
        if (empty($tables)) {
            $tables = $this->_getTablesList();
        }

        foreach ($tables as $table_name) {
            $this->logs[] = "[$table_name] start ...";

            $schema = $this->_getTableSchema($table_name);
            $bookmark_key = $this->_getBookmarkKey($table_name, $schema);
            $fields_list = array_column($schema['fields'], 'name');

            $tableExists = $this->dataset->table($table_name)->exists();

            /**
             * If table should be excluded and exists, delete it from BG.
             */
            $shouldBeExcluded = in_array($table_name, $excluded_tables);
            if ($shouldBeExcluded) {
                if ($tableExists && !in_array($table_name, $this->config['do_not_delete_excluded'])) {
                    $this->logs[] = "[$table_name] deleting, should not exist ...";
                    $this->dataset->table($table_name)->delete();
                }
                continue;
            }

            /*
             * If table exists and schema changed, delete it
             */
            if ($tableExists) {
                $table_info = $this->dataset->table($table_name)->info();
                $schemaChanged = $this->_checkSchemaChanged($schema, $table_info['schema']);
                if ($schemaChanged) {
                    $this->logs[] = "[$table_name] deleting, schema changed ...";
                    $this->dataset->table($table_name)->delete();
                    $tableExists = false;
                }
            }

            /*
             * If table does not exist, create it
             */
            if (!$tableExists) {
                $this->logs[] = "[$table_name] creating ...";
                $this->dataset->createTable($table_name, ['schema' => $schema]);
            }

            /*
             * Starting loop
             */
            $this->logs[] = "[$table_name] starting json chunks import ...";


            DB::connection($this->config['database_connection_name'])
                ->table($table_name)
                ->orderBy($this->_getSortColumn($table_name, $bookmark_key))
                ->chunk($chunks, function ($data_part) use ($table_name, $schema, $fields_list) {
                    $this->logs[] = count($data_part).' records found.';

                    /**
                     * Import data to temp table
                     */
                    $nd_json = '';
                    foreach ($data_part as $item) {
                        $nd_json .= json_encode($item, JSON_UNESCAPED_UNICODE)."\r\n";
                    }

                    $are_data_loaded = $this->tryToLoadData($table_name, '', $nd_json);

                    /**
                     * Merge data into
                     */
                    if (!$are_data_loaded) {
                        $this->logs[] = "BigQuery: Error before merging $table_name";
                        \Log::error("BigQuery: Error before merging $table_name");
                    }
                });

            $this->logs[] = "[$table_name] end ...";
        }

        $this->logs[] = 'Job ends here.';
    }

    /**
     * Execute the console command.
     *
     * @param  array  $tables
     * @param  bool  $reset_tables
     */
    public function pushCollection($data, $schema, $destinationTable)
    {

        $tableExists = $this->dataset->table($destinationTable)->exists();

        if ($tableExists) {
            $this->logs[] = "[$destinationTable] deleting, should not exist ...";
            $this->dataset->table($destinationTable)->delete();
        }

        $this->logs[] = "[$destinationTable] creating ...";
        $this->dataset->createTable($destinationTable, ['schema' => $schema]);

        $chunks = 150000;

        foreach($data->chunk($chunks) as $data_part) {

            $this->logs[] = count($data_part).' records found.';

            /**
             * Import data to temp table
             */
            $nd_json = '';
            foreach ($data_part as $item) {
                $nd_json .= json_encode($item, JSON_UNESCAPED_UNICODE)."\r\n";
            }

            $are_data_loaded = $this->tryToLoadData($destinationTable, '', $nd_json);

            /**
             * Merge data into
             */
            if (!$are_data_loaded) {
                $this->logs[] = "BigQuery: Error before merging $destinationTable";
                Log::error("BigQuery: Error before merging $destinationTable");
            }
        }

        $this->logs[] = "[$destinationTable] end ...";


        $this->logs[] = 'Job ends here.';
    }

    /**
     * Delete tables from BQ.
     *
     * @param $tables
     */
    public function deleteTables($tables)
    {
        foreach ($tables as $table_name) {
            if ($this->dataset->table($table_name)->exists()) {
                $this->dataset->table($table_name)->delete();
            }
        }
    }

    /**
     * @param $table_name
     * @param  array  $schema
     * @return string
     */
    private function _getBookmarkKey($table_name, array $schema = [])
    {
        if (isset($this->config['table_meta'][$table_name]['bookmark_key'])) {
            return $this->config['table_meta'][$table_name]['bookmark_key'];
        }

        return array_search('updated_at', array_column($schema['fields'], 'name')) === false
            ? 'id'
            : 'updated_at';
    }

    /**
     * @param  string $table_name
     * @param  string  $bookmark_key
     * @return string
     */
    private function _getSortColumn($table_name, $bookmark_key)
    {
        if (isset($this->config['table_meta'][$table_name]['sort_column'])) {
            return $this->config['table_meta'][$table_name]['sort_column'];
        }
        return $bookmark_key;
    }

    /**
     * We implement this logic - to sync rows that where updated some minutes ago - as some rows were never synced, or had missing information
     * @param $table_name
     * @return string
     */
    private function _getOffsetConfig($table_name)
    {
        $default = [
            'time' => '5 minutes ago',
            'column' => 'updated_at',
        ];

        if (!isset($this->config['table_meta'][$table_name]['time_offset'])) {
            return $default;
        }

        if ($this->config['table_meta'][$table_name]['time_offset'] === false) {
            return false;
        }

        return $this->config['table_meta'][$table_name]['time_offset'] + $default;
    }

    /**
     * Load csv into BigQuery Merge Table.
     *
     * @param $table_name
     * @param $table_suffix
     * @param $content
     * @param  string  $type
     * @return bool
     * @throws \Exception
     */
    private function loadDataIntoBigquery($table_name, $table_suffix, $content, $type = 'nd_json')
    {
        $table = $this->dataset->table($table_name.$table_suffix);
        if ($type === 'nd_json') {
            $loadConfig = $table->load($content)->sourceFormat('NEWLINE_DELIMITED_JSON');
        } else {
            $loadConfig = $table->load($content)->sourceFormat('CSV');
        }
        $job = $table->runJob($loadConfig);

        $backoff = new ExponentialBackoff(10);
        $backoff->execute(function () use ($job, $table_name) {
            $this->logs[] = "[$table_name] waiting for job to complete";
            $job->reload();
            if (!$job->isComplete()) {
                throw new \Exception('Job has not yet completed', 500);
            }
        });

        if (isset($job->info()['status']['errorResult'])) {
            $error = $job->info()['status']['errorResult']['message'];
            $this->logs[] = "[$table_name] ERROR $error";
            foreach ($job->info()['status']['errors'] as $error_item) {
                $this->logs[] = "[$table_name] > ".$error_item['message'];
            }

            return false;
        } else {
            return true;
        }
    }

    /**
     * Merge Tmp with Live table in BigQuery.
     *
     * @param $dataset_id
     * @param $table_name
     * @param $table_suffix
     * @param $fields_list
     */
    private function runBqMerge($table_name, $table_suffix, $fields_list)
    {
        $fields_str = '`'.implode('`, `', $fields_list).'`';
        $update_sql = $this->getUpdateFields($fields_list);

        $main_table = "{$this->config['dataset_id']}.{$table_name}";
        $tmp_table = $main_table.$table_suffix;

        $mergeQuery = BigQueryFacade::query("
            MERGE $main_table T
            USING $tmp_table S
            ON T.id = S.id
            WHEN MATCHED THEN
              UPDATE SET $update_sql
            WHEN NOT MATCHED THEN
              INSERT ($fields_str) VALUES ($fields_str)
            ");

        try {
            BigQueryFacade::runQuery($mergeQuery);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Format SQL update query.
     * @param $fields
     * @param $exclude
     * @return string
     */
    private function getUpdateFields($fields, $exclude = [])
    {
        $sql = [];
        foreach ($fields as $field) {
            if (!in_array($field, $exclude)) {
                $sql[] = "`$field` = `S`.`$field`";
            }
        }

        return implode(', ', $sql);
    }

    /**
     * Get the last entry key, in order to select the appropriate data to update.
     *
     * @param $table_name
     * @param  string  $key
     * @return null
     */
    private function getLastEntry($table_name, $key = 'updated_at')
    {
        $query = BigQueryFacade::query("SELECT MAX({$key}) as res FROM {$this->config['dataset_id']}.{$table_name}");
        $queryResults = BigQueryFacade::runQuery($query);

        foreach ($queryResults as $row) {
            $res = $row['res'];
        }

        return $res ?? null;
    }

    /**
     * Check if table schema has changed.
     *
     * @param $local_schema
     * @param $remote_schema
     * @return bool
     */
    private function _checkSchemaChanged($local_schema, $remote_schema)
    {
        $local_schema['fields'] = collect($local_schema['fields'])->map(function ($item) {
            $item['type'] = $this->_mapColumnTypeDiffLocalRemote($item['type']);

            return $item;
        })->toArray();

        return $local_schema['fields'] !== $remote_schema['fields'];
    }

    /**
     * Returns the table list of current DB.
     *
     * @return mixed
     */
    private function _getTablesList()
    {
        return collect(DB::connection($this->config['database_connection_name'])->select("SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'"))->pluck('table_name');
        // return DB::connection($this->config['database_connection_name'])->getDoctrineSchemaManager()->listTableNames();
    }

    /**
     * Creates the schema for the given table, in a BigQuery supported format.
     *
     * @param $table
     * @return array
     */
    private function _getTableSchema($table)
    {
        /**
         * Read columns details from information_schema.
         */
        $columns = $this->_getColumns($table);

        /**
         * Create schema.
         */
        $fields = [];
        foreach ($columns as $column) {
            $tmp_col = [
                'name' => $this->_getColumnName($column),
                'type' => $this->_getColumnType($column),
            ];

            $is_nullable = $this->_isColumnNullable($column);
            if ($is_nullable === 'YES') {
                $tmp_col['mode'] = 'NULLABLE';
            }
            $fields[] = $tmp_col;
        }

        return ['fields' => $fields];
    }

    /**
     * Returns table columns.
     *
     * @param $table
     * @return mixed
     */
    private function _getColumns($table)
    {
        if ($this->config['database_driver'] == 'mysql') {
            $schema_name = DB::connection($this->config['database_connection_name'])->getDatabaseName();

            return DB::connection($this->config['database_connection_name'])->select("
                select `column_name`, `data_type`, `is_nullable`
                from information_schema.columns 
                where `table_schema` = '$schema_name' 
                and `table_name` = '$table' 
                order by `ordinal_position`
            ");
        }

        return DB::connection($this->config['database_connection_name'])->select("
            select column_name, data_type, is_nullable
            from information_schema.columns
            where table_name = '$table'
            order by ordinal_position
        ");
    }

    /**
     * Returns if column is nullable.
     *
     * @param $column
     * @return mixed
     */
    private function _isColumnNullable($column)
    {
        if ($this->config['database_driver'] == 'mysql' && $this->_getMysqlVersion() != '5.7') {
            return $column->IS_NULLABLE;
        }

        return $column->is_nullable;
    }

    /**
     * Returns the column name.
     *
     * @param $column
     * @return mixed
     */
    private function _getColumnName($column)
    {
        if ($this->config['database_driver'] == 'mysql' && $this->_getMysqlVersion() != '5.7') {
            return $column->COLUMN_NAME;
        }

        return $column->column_name;
    }

    /**
     * Get MySQL version.
     *
     * @return bool|string
     */
    private function _getMysqlVersion()
    {
        $mysql_version_plain = DB::connection($this->config['database_connection_name'])->select('SELECT VERSION() as version;');

        return substr($mysql_version_plain[0]->version, 0, 3);
    }

    /**
     * Maps the column type with those supported at BigQuery.
     *
     * @param $column
     * @return string
     */
    private function _getColumnType($column)
    {
        $function = '_mapColumnType'.ucfirst($this->config['database_driver']);

        return $this->{$function}($column);
    }

    /**
     * Maps the column type with those supported at BigQuery.
     * MySQL Mapping.
     *
     * @param $column
     * @return string
     */
    private function _mapColumnTypeMysql($column)
    {
        $type = $this->_getMysqlVersion() == '5.7' ? $column->data_type : $column->DATA_TYPE;
        switch ($type) :
            case 'tinyint':
            case 'smallint':
            case 'bigint':
            case 'mediumint':
            case 'int':
                $bigquery_type = 'INTEGER';
                break;
            case 'date':
                $bigquery_type = 'DATE';
                break;
            case 'numeric':
                $bigquery_type = 'NUMERIC';
                break;
            case 'float':
                $bigquery_type = 'FLOAT64';
                break;
            case 'timestamp':
            case 'datetime':
                $bigquery_type = 'DATETIME';
                break;
            default:
                $bigquery_type = 'STRING';
                break;
        endswitch;

        return $bigquery_type;
    }

    /**
     * Maps the column type with those supported at BigQuery.
     * PgSQL Mapping.
     *
     * @param $column
     * @return string
     */
    private function _mapColumnTypePgsql($column)
    {
        $type = $column->data_type;

        switch ($type) :
            case 'smallint':
            case 'bigint':
            case 'integer':
            case 'mediumint':
                $bigquery_type = 'INTEGER';
                break;
            case 'date':
                $bigquery_type = 'DATE';
                break;
            case 'boolean':
                $bigquery_type = 'BOOLEAN';
                break;
            case 'numeric':
                $bigquery_type = 'NUMERIC';
                break;
            case 'double precision':
                $bigquery_type = 'FLOAT64';
                break;
            case 'timestamp without time zone':
                $bigquery_type = 'TIMESTAMP';
                break;
            default:
                $bigquery_type = 'STRING';
                break;
        endswitch;

        return $bigquery_type;
    }

    private function _mapColumnTypeDiffLocalRemote($type)
    {
        switch ($type) :
            case 'FLOAT64':
                return 'FLOAT';
                break;
            default:
                return $type;
                break;
        endswitch;
    }

    private function deleteTable($table_name)
    {
        if ($this->dataset->table($table_name)->exists()) {
            $this->logs[] = "[$table_name] deleting ...";
            $this->dataset->table($table_name)->delete();
        }
    }

    private function createTable($table_name, $schema)
    {
        $this->logs[] = "[$table_name] creating ...";
        $this->dataset->createTable($table_name, ['schema' => $schema]);
    }

    private function tryToLoadData($table_name, $table_suffix, $nd_json, $tries = 3)
    {
        $success = false;
        while ($tries > 0 && !$success) {
            $success = $this->loadDataIntoBigquery($table_name, $table_suffix, $nd_json);
            $tries--;
        }

        return $success;
    }

    private function tryToMerge($table_name, $table_suffix, $fields_list, $tries = 3)
    {
        $success = false;
        while ($tries > 0 && !$success) {
            $success = $this->runBqMerge($table_name, $table_suffix, $fields_list);
            $tries--;
        }

        return $success;
    }
}
