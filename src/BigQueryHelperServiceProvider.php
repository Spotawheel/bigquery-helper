<?php

namespace Spotawheel\BigQueryHelper;

use Illuminate\Contracts\Http\Kernel;
use Illuminate\Support\ServiceProvider;

class BigQueryHelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application events.
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/bigquerycnfg.php' => config_path('bigquerycnfg.php'),
        ]);
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/bigquerycnfg.php', 'bigquerycnfg');

        $this->app->singleton(BigQueryHelper::class);

        $this->app->alias(BigQueryHelper::class, 'bigqueryhelper');

    }

}
