# Changelog

All changes to `spotawheel/bigquery-helper` will be documented in this file.

## 2021-07-02
- Bug fixes

## 2021-06-15
- Add option to set chunk by table

## 2021-03-05
- Added push() function, which is similar to sync(), but it just inserts new rows
- Added new config options for tables (sort column and time offset) 

## 2021-02-26
- Require dev-master branch of schulzefelix/laravel-bigquery

## 2021-02-09
- Added functionality to create dataset when not exist
- Require forked version of schulzefelix/laravel-bigquery in order to set location for datasets 
