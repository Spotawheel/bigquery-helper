<?php

return [
    /*
    |--------------------------------------------------------------------------
    | BigQuery Dataset ID
    |--------------------------------------------------------------------------
     */
    'dataset_id' => env('BQ_DATASET_ID', null),

    /*
    |--------------------------------------------------------------------------
    | BigQuery Project ID
    |--------------------------------------------------------------------------
     */
    'project_id' => env('GOOGLE_CLOUD_PROJECT_ID'),

    /*
    |--------------------------------------------------------------------------
    | Excluded Tables
    |--------------------------------------------------------------------------
    |
    | Tables that should be excluded from sync. Sync will delete all excluded tables from BigQuery
    | if they exist. If you want to keep the data in BigQuery, add the table name to the do_not_delete_excluded array.
    |
    */
    'excluded_tables' => [
        'migrations',
        'password_resets',
        'model_has_permissions',
        'model_has_roles',
        'permissions',
        'roles',
        'role_has_permissions',
        'failed_jobs',
        'oauth_refresh_tokens',
        'oauth_auth_codes',
        'classified_list_job',
        'list_jobs',
    ],

    'do_not_delete_excluded' => [],

    /*
    |--------------------------------------------------------------------------
    | Table Meta
    |--------------------------------------------------------------------------
    |
    | Table specific settings, i.e. which is the bookmark key for each table
    | bookmark_key : default value is updated_at or - when updated_at does not exist - id
    | sort_column : default value is id, the table will be sorted by this column
    | time_offset : DO NOT EDIT THIS unless you know what you are doing! - false disables this
    | chunks : chunk results by X rows, default 100000
    |
    */
    'table_meta' => [
        'users' => [
            'bookmark_key' => 'updated_at',
            'sort_column' => 'id',
            'time_offset' => [
                'time' => '5 minutes ago',
                'column' => 'updated_at'
            ],
            'chunks' => 150000,
        ],
    ],
];
